#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import csv
import calcoohija

if __name__ == "__main__":
    numero1 = sys.argv[1]
    numero2 = numero1
    calcular = calcoohija.CalculadoraHija(numero1, numero2)

    with open(sys.argv[1]) as fichero:
        lineas = csv.reader(fichero)

        for linea in lineas:
            if linea[0] == "suma":
                suma = calcular.suma(int(linea[1]), int(linea[2]))
                for ultimoelemento in linea[3:]:
                    suma = calcular.suma(int(suma), int(ultimoelemento))
                print(suma)

            elif linea[0] == "resta":
                resta = calcular.resta(int(linea[1]), int(linea[2]))
                for ultimoelemento in linea[3:]:
                    resta = calcular.resta(int(resta), int(ultimoelemento))
                print(resta)

            elif linea[0] == "multiplica":
                multiplica = calcular.multiplicacion(int(linea[1]),
                                                     int(linea[2]))
                for ultimoelemento in linea[3:]:
                    multiplica = calcular.multiplicacion(int(multiplica),
                                                         int(ultimoelemento))
                print(multiplica)

            elif linea[0] == "divide":
                try:
                    divide = calcular.division(int(linea[1]),
                                               int(linea[2]))
                    for ultimoelemento in linea[3:]:
                        divide = calcular.division(int(divide),
                                                   int(ultimoelemento))
                    print(divide)
                except ZeroDivisionError:
                    sys.exit("Division by zero is nor allowed")
