#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import ca


class CalculadoraHija(calcoo.Calculadora):

    def multiplicacion(self, numero1, numero2):
        """Función multiplicación"""
        return numero1 * numero2

    def division(self, numero1, numero2):
        """Función división"""
        return numero1 / numero2


if __name__ == "__main__":
    try:
        numero1 = int(sys.argv[1])
        numero2 = int(sys.argv[3])
        calcular = CalculadoraHija(numero1, numero2)

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        resultado = calcular.suma(numero1, numero2)

    elif sys.argv[2] == "resta":
        resultado = calcular.resta(numero1, numero2)

    elif sys.argv[2] == "multiplicacion":
        resultado = calcular.multiplicacion(numero1, numero2)

    elif sys.argv[2] == "division":
        try:
            resultado = calcular.division(numero1, numero2)
        except ZeroDivisionError:
            sys.exit("Division by zero is nor allowed")

    else:
        sys.exit('Sólo puedes sumar, restar, multiplicacion o division.')

    print(resultado)
