#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija


if __name__ == "__main__":
    fichero = sys.argv[1]
    fich = open('fichero')
    lineas = fich.readlines()
    numero1 = sys.argv[1]
    numero2 = numero1
    calcular = calcoohija.CalculadoraHija(numero1, numero2)

    for linea in lineas:
        elementos = linea.split(',')
        ultimoelemento = len(elementos)

        if elementos[0] == "suma":
            suma = calcular.suma(int(elementos[1]), int(elementos[2]))
            for ultimoelemento in elementos[3:]:
                suma = calcular.suma(int(suma), int(ultimoelemento))
            print(suma)

        elif elementos[0] == "resta":
            resta = calcular.resta(int(elementos[1]), int(elementos[2]))
            for ultimoelemento in elementos[3:]:
                resta = calcular.resta(int(resta), int(ultimoelemento))
            print(resta)

        elif elementos[0] == "multiplica":
            multiplica = calcular.multiplicacion(int(elementos[1]),
                                                 int(elementos[2]))
            for ultimoelemento in elementos[3:]:
                multiplica = calcular.multiplicacion(int(multiplica),
                                                     int(ultimoelemento))
            print(multiplica)

        elif elementos[0] == "divide":
            try:
                divide = calcular.division(int(elementos[1]),
                                           int(elementos[2]))
                for ultimoelemento in elementos[3:]:
                    divide = calcular.division(int(divide),
                                               int(ultimoelemento))
                print(divide)
            except ZeroDivisionError:
                sys.exit("Division by zero is nor allowed")

        else:
            sys.exit()

    fich.close()
