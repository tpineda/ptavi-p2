#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():
    def __init__(self, numero1, numero2):
        self.numero1 = numero1
        self.numero2 = numero2

    def suma(self, numero1, numero2):
        """Función suma"""
        return numero1 + numero2

    def resta(self, numero1, numero2):
        """Función resta"""
        return numero1 - numero2


if __name__ == "__main__":
    try:
        numero1 = int(sys.argv[1])
        numero2 = int(sys.argv[3])
        calcular = Calculadora(numero1, numero2)

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        resultado = calcular.suma(numero1, numero2)

    elif sys.argv[2] == "resta":
        resultado = calcular.resta(numero1, numero2)

    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(resultado)
